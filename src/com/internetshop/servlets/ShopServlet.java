package com.internetshop.servlets;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.internetshop.product.Product;
import com.internetshop.session.IManageShopSessionBean;

/**
 * Servlet implementation class ShopServlet
 */
@WebServlet("/ShopServlet")
public class ShopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@EJB
	private IManageShopSessionBean manageShopSessionBeanLocal;

	public void getProdutsListFromDB(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int currentPage = 1;
		int recordsPerPage = 4;
		if (request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}

		List<Product> products = manageShopSessionBeanLocal.getProductsList((currentPage - 1) * recordsPerPage, recordsPerPage);
		if (products.isEmpty()) {
			request.setAttribute("technicalPromlemsMessage", "Sorry! We have some technical problems. Try to login later");
			RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
			rd.forward(request, response);
		} else {
			int noOfRecords = (int) manageShopSessionBeanLocal.getNoOfDBRecords();
			int pageNumber = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
			request.setAttribute("products", products);
			request.setAttribute("pageNumber", pageNumber);
			request.setAttribute("currentPage", currentPage);
			RequestDispatcher rd = request.getRequestDispatcher("shop.jsp");
			rd.forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		if (request.getParameter("logout") != null) {
			session.removeAttribute("isOnline");
			response.sendRedirect("login.jsp");
		}

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		HttpSession session = request.getSession(true);

		if (session.getAttribute("isOnline") == null) {
			request.setAttribute("UserNotLoginMessage", "First login!");
			RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
			rd.forward(request, response);
			return;
		}
		if (session.getAttribute("firstLogOut") != null) {
			request.setAttribute("firstLogOutMessage", "First log out!");
			getProdutsListFromDB(request, response);
			return;
		}

		if (session.getAttribute("isOnline") != null || request.getAttribute("goToProductsPage") != null) {
			getProdutsListFromDB(request, response);
		}
	}

}
