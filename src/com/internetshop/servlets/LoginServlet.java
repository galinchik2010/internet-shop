package com.internetshop.servlets;

import java.io.IOException; 

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.internetshop.sequrity.MD5Encryption;
import com.internetshop.session.IManageUserSessionBean;
import com.internetshop.user.User;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	private IManageUserSessionBean manageUserSessionBeanLocal;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		HttpSession session = request.getSession(true);

		String username = request.getParameter("username");
		String password = request.getParameter("password");

		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
	
		try {
			if (!manageUserSessionBeanLocal.checkLoginData(username, MD5Encryption.MD5(password))) {
				request.setAttribute("wrongLoginDataMessage", "Wrong username or password! Try to login again.");
				request.setAttribute("loginData", user);
				RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
				rd.forward(request, response);
			} else {
				session.setAttribute("id", manageUserSessionBeanLocal.getUserId(username));
				session.setAttribute("isOnline", "isOnline");
				response.sendRedirect("ShopServlet");

			}
		} catch (Exception e) {
			request.setAttribute("technicalProblemsMessage", "Sorry! We have some technical promlems. Try to login later");
			RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
			rd.forward(request, response);
		}

	}

}
