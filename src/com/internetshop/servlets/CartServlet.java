package com.internetshop.servlets;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.jms.JMSException;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.internetshop.message.IProducer;
import com.internetshop.message.MessageEntity;
import com.internetshop.product.Product;
import com.internetshop.session.IManageCartSessionBean;
import com.internetshop.session.IManageUserSessionBean;
import com.internetshop.user.User;

/**
 * Servlet implementation class CartServlet
 */
@SuppressWarnings("serial")
@WebServlet("/CartServlet")
public class CartServlet extends HttpServlet {
	private static final String SHOPPING_CART_BEAN_SESION_KEY = "shoppingCart";

	private Product product;

	@EJB
	private IProducer producer;

	@EJB
	private IManageCartSessionBean shoppingCartBean;

	@EJB
	private IManageUserSessionBean userDataBean;


	private IManageCartSessionBean getCart(HttpServletRequest request) {

		IManageCartSessionBean shoppingCartBeanLocal = (IManageCartSessionBean) request.getSession().getAttribute(SHOPPING_CART_BEAN_SESION_KEY);

		if (shoppingCartBeanLocal == null) {
			InitialContext ic;
			try {
				ic = new InitialContext();
				shoppingCartBean = (IManageCartSessionBean) ic.lookup("java:global/InternetShop/ManageCartSessionBeanImpl!com.internetshop.session.IManageCartSessionBean");

			} catch (NamingException e) {
				e.printStackTrace();
			}
			request.getSession().setAttribute(SHOPPING_CART_BEAN_SESION_KEY, shoppingCartBean);
			return shoppingCartBean;

		} else
			return shoppingCartBeanLocal;

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");

		IManageCartSessionBean shoppingCartBeanLocal = getCart(request);
		HttpSession session = request.getSession(true);
		if (request.getParameter("deleteProductsList")!=null){
			shoppingCartBeanLocal.removeProductsList();
			session.removeAttribute("cartProductsList");
		}
		if (request.getParameter("id") != null) {

			int id = Integer.parseInt(request.getParameter("id"));
			product = shoppingCartBeanLocal.getProductFromDBbyId(id);
			if ((product != null) && (!shoppingCartBeanLocal.isProductInProductsCartList(id))) {
				shoppingCartBeanLocal.productQuantityMap(id, 1);
				session.setAttribute("ProductQuantityMap", shoppingCartBeanLocal.getProductQuantityMap());
				shoppingCartBeanLocal.addProductToProductsCartList(product);

				String referer = request.getHeader("Referer");
				response.sendRedirect(referer);
			} else if (shoppingCartBeanLocal.isProductInProductsCartList(id)) {
				shoppingCartBeanLocal.productQuantityMap(id, 1);
				session.setAttribute("ProductQuantityMap", shoppingCartBeanLocal.getProductQuantityMap());
				String referer = request.getHeader("Referer");
				response.sendRedirect(referer);
			}
		}
		if (request.getParameter("goToCartPage") != null) {
			session.setAttribute("cartProductsList", shoppingCartBeanLocal.getProductsList());
			response.sendRedirect("cart.jsp");
		} 

		if (request.getParameter("removeById") != null) {

			int id = Integer.parseInt(request.getParameter("removeById"));
			shoppingCartBeanLocal.removeProductFromProductsCartList(id);
			shoppingCartBeanLocal.deleteProductQuantityFromMap(id);
			session.setAttribute("cartProductsList", shoppingCartBeanLocal.getProductsList());
			response.sendRedirect("cart.jsp");

		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		HttpSession session = request.getSession(true);
		if (request.getParameter("checkout") != null) {

			IManageCartSessionBean shoppingCartBeanLocal = getCart(request);

			List<Product> productsList = shoppingCartBeanLocal.getProductsList();
			User user = userDataBean.getUserDataById((int) session.getAttribute("id"));
			Map<Integer, Integer> quantitiesMap = shoppingCartBeanLocal.getProductQuantityMap();
			List<Integer> quantities = shoppingCartBeanLocal.getProductQuantityList(quantitiesMap);
			MessageEntity messEntity = new MessageEntity();
			messEntity.setMessage(user, quantities, productsList);
			messEntity.setEmail(user.getEmail());
			try {
				producer.sendMessage(messEntity);
				shoppingCartBeanLocal.removeProductsList();
				session.removeAttribute("cartProductsList");
				request.setAttribute("successfulPurchase", "Thank you for your purchase! Purchase details were sent to your email");
				RequestDispatcher rd = request
						.getRequestDispatcher("cart.jsp");
				rd.forward(request, response);
			} catch (JMSException e) {
				e.printStackTrace();
			}

		}
	}

}
