package com.internetshop.servlets;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.internetshop.sequrity.MD5Encryption;
import com.internetshop.session.IManageUserSessionBean;
import com.internetshop.user.User;

/**
 * Servlet implementation class RegistrationServlet
 */
@WebServlet("/RegistrationServlet")
public class RegistrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	private IManageUserSessionBean manageUserSessionBeanLocal;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String email = request.getParameter("email");
		String firstname = request.getParameter("firstname");
		String lastname = request.getParameter("lastname");

		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		user.setFirstname(firstname);
		user.setLastname(lastname);
		user.setEmail(email);
		try {
			if (!manageUserSessionBeanLocal.checkRegistrationData(username)) {
				request.setAttribute("wrongRegistrationDataMessage", "User with same username already exists! Check another username.");
				request.setAttribute("user", user);
				RequestDispatcher rd = request.getRequestDispatcher("registration.jsp");
				rd.forward(request, response);
			} else {
				user.setPassword(MD5Encryption.MD5(password));
				if (manageUserSessionBeanLocal.createUser(user)) {
					request.setAttribute("successfulRegistrationMessage", "User Successfuly Added");
					RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
					rd.forward(request, response);
				}
			}
		} catch (Exception e) {
			request.setAttribute("technicalProblemsMessage", "Sorry! We have some technical problems. Try to register later");
			RequestDispatcher rd = request.getRequestDispatcher("registration.jsp");
			rd.forward(request, response);
		}

	}

}
