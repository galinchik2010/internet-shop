package com.internetshop.message;

import javax.annotation.Resource; 
import javax.ejb.Stateless;
import javax.jms.*;

@Stateless
public class Producer implements IProducer 
{
	@Resource(name = "MyJmsConnectionFactory")
	private ConnectionFactory connectionFactory;

	@Resource(name = "Queue")
	private Destination destination;
	
	@Override
	public void sendMessage(MessageEntity messEntity) throws JMSException
	{
		Connection connection = connectionFactory.createConnection();
		Session session = connection.createSession(true,
				Session.AUTO_ACKNOWLEDGE);

		connection.start();
		MessageProducer producer = session.createProducer(destination);
		
		
		ObjectMessage objectmessage = session.createObjectMessage(messEntity);
		producer.send(objectmessage);

		producer.close();
		session.close();
		connection.close();
	}
}