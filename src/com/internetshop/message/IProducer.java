package com.internetshop.message;

import javax.ejb.Local;
import javax.jms.JMSException;

@Local
public interface IProducer {
	public abstract void sendMessage(MessageEntity messEntity) throws JMSException;
}