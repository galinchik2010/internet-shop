package com.internetshop.message;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

@MessageDriven(activationConfig = { @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"), @ActivationConfigProperty(propertyName = "destination", propertyValue = "Queue") })
public class QueueListenerMDB implements MessageListener {

	private static final String SMTP_HOST = "smtp.mail.ru";
	private static final String SMTP_PORT = "465";
	private static final String SENDER_EMAIL = "g.mudrik@mail.ru";
	private static final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";

	public QueueListenerMDB() {
	}

	public void onMessage(Message message) {
		MessageEntity objmsg = null;
		try {
			if (message instanceof ObjectMessage) {
				objmsg = (MessageEntity) ((ObjectMessage) message).getObject();
				send("Hello from Internet shop", objmsg.getMessage(), objmsg.getEmail());
			} else {
				System.out.println("Not valid message for this Queue MDB");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void send(String title, String content, String recipient) throws AddressException, MessagingException {

		java.util.Properties properties = System.getProperties();
		properties.setProperty("mail.smtp.host", SMTP_HOST);
		properties.put("mail.smtp.port", SMTP_PORT);
		properties.put("mail.smtp.socketFactory.port", SMTP_PORT);
		properties.put("mail.smtp.socketFactory.class", SSL_FACTORY);
		properties.put("mail.smtp.socketFactory.fallback", "false");
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.user", "g.mudrik@mail.ru");
		properties.put("mail.smtp.password", "angel100");

		Session session = Session.getDefaultInstance(properties, new javax.mail.Authenticator() {

			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("g.mudrik@mail.ru", "angel100");
			}
		});

		// Construct the message
		javax.mail.Message msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress(SENDER_EMAIL));
		msg.setRecipient(javax.mail.Message.RecipientType.TO, new InternetAddress(recipient));
		msg.setSubject(title);
		msg.setText(content);
		Transport.send(msg);
	}
}