package com.internetshop.message;

import java.io.Serializable;
import java.util.List;

import com.internetshop.product.Product;
import com.internetshop.user.User;

@SuppressWarnings("serial")
public class MessageEntity implements Serializable {

	private String message;
	private String username;
	private String product;
	private String email;
	private float price;
	private int quantity;
	private float purchase;
	private String priceList = "";

	public void setMessage(User user, List<Integer> quantities, List<Product> products) {
		this.username = user.getFirstname() + " " + user.getLastname();
		for (int i = 0; i < products.size(); i++) {
			this.product = products.get(i).getName();
			this.price = Float.parseFloat(products.get(i).getPrice());
			this.quantity = quantities.get(i);
			priceList = priceList + "\n" + (i + 1) + "." + this.product + ": price - " + this.price + ", quantity - " + this.quantity;
			purchase = purchase + this.quantity * this.price;
			this.message = "Dear " + username + "\nThank you for purchasing. \nYour purchase is " + priceList + "\nTotal price is " + purchase;
		}
	}

	public String getMessage() {
		return message;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmail() {
		return email;
	}

}
