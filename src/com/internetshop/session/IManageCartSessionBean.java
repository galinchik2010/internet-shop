package com.internetshop.session;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import com.internetshop.product.Product;

@Local
public interface IManageCartSessionBean {

	public Product getProductFromDBbyId(int id);

	public void addProductToProductsCartList(Product product);

	public void removeProductFromProductsCartList(int id);

	public List<Product> getProductsList();
	
	public void removeProductsList();

	public boolean isProductInProductsCartList(int id);

	public void productQuantityMap(int id, int count);

	public Map<Integer, Integer> getProductQuantityMap();

	public List<Integer> getProductQuantityList(Map<Integer, Integer> quantityCounter);

	public void deleteProductQuantityFromMap(int id);

}
