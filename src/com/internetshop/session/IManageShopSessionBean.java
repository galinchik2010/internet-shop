package com.internetshop.session;

import java.util.List;

import javax.ejb.Local;

import com.internetshop.product.Product;

@Local
public interface IManageShopSessionBean {

	public List<Product> getProductsList(int offset, int noOfRecords);

	public long getNoOfDBRecords();
}
