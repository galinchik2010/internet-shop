package com.internetshop.session;

import java.util.List; 

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.internetshop.user.User;

@Stateless
public class ManageUserSessionBeanImpl implements IManageUserSessionBean {

	@PersistenceContext(unitName = "InetShop")
	private EntityManager manager;

	@Override
	public boolean createUser(User user) {
		manager.persist(user);
		return true;
	}

	public boolean checkRegistrationData(String username){
		Query query = manager.createQuery("select c from User c WHERE c.username LIKE :username").setParameter("username", username);
		@SuppressWarnings("unchecked")
		List<User> resultList = query.getResultList();
		if (resultList.isEmpty()) {
			return true;
		} else {
			return false;
		}

	}

	public boolean checkLoginData(String username, String password)  {
		Query query = manager.createQuery("select c from User c WHERE c.username LIKE :username AND c.password LIKE :password").setParameter("username", username).setParameter("password", password);
		@SuppressWarnings("unchecked")
		List<User> resultList = query.getResultList();
		if (resultList.isEmpty()) {
			return false;
		} else {
			return true;
		}
	}

	public User getUserDataById(int id) {
		User userData = manager.find(User.class, id);
		return userData;

	}

	public int getUserId(String username) {
		Query query = manager.createQuery("select c from User c WHERE c.username LIKE :username").setParameter("username", username);
		User userData = (User) query.getSingleResult();

		return userData.getId();

	}

}