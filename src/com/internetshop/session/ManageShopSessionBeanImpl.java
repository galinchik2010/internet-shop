package com.internetshop.session;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.internetshop.product.Product;

@Stateless
public class ManageShopSessionBeanImpl implements IManageShopSessionBean {

	@PersistenceContext(unitName = "InetShop")
	private EntityManager manager;

	@SuppressWarnings("unchecked")
	public List<Product> getProductsList(int offset, int limit) {

		Query query = manager.createQuery("select c from Product c").setFirstResult(offset).setMaxResults(limit);
		List<Product> resultList = query.getResultList();
		return resultList;

	}

	public long getNoOfDBRecords() {
		Query query = manager.createQuery("select COUNT(c) from Product c");
		long noOfRecords = (long) query.getSingleResult();

		return noOfRecords;
	}

}
