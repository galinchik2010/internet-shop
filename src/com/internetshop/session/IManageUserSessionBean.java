package com.internetshop.session;

import javax.ejb.Local;

import com.internetshop.user.User;

@Local
public interface IManageUserSessionBean {
	
	public boolean checkRegistrationData(String username);
	public boolean createUser(User user);
	public boolean checkLoginData(String username, String password) ;
	public User getUserDataById(int id);
	public int getUserId(String username);
}
