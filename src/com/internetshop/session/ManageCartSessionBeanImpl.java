package com.internetshop.session;

import java.util.ArrayList; 
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.internetshop.product.Product;

@Stateful

public class ManageCartSessionBeanImpl implements IManageCartSessionBean {

	@PersistenceContext(unitName = "InetShop")
	private EntityManager manager;
	private List<Product> products;
	private Map<Integer, Integer> quantityCounterMap;

	@PostConstruct
	public void init() {
		quantityCounterMap = new HashMap<Integer, Integer>();
		products = new ArrayList<Product>();
	}

	public Product getProductFromDBbyId(int id) {

		Product product = manager.find(Product.class, id);
		return product;
	}

	public void addProductToProductsCartList(Product product) {
		products.add(product);
	}

	public void removeProductFromProductsCartList(int id) {
		Iterator<Product> it = products.iterator();
		while (it.hasNext()) {
			if (it.next().getId() == (id)) {
				it.remove();
			}
		}
	}

	public List<Product> getProductsList() {
		return products;
	}

	public void removeProductsList() {
		products.clear();;
	}

	public boolean isProductInProductsCartList(int id) {
		for (Product prod : products) {
			if (prod.getId() == id) {
				return true;
			}

		}
		return false;
	}

	public void productQuantityMap(int id, int count) {
		if (quantityCounterMap.containsKey(id)) {
			for (Map.Entry<Integer, Integer> entry : quantityCounterMap.entrySet()) {
				if (entry.getKey() == id) {
					Integer key = entry.getKey();
					Integer value = entry.getValue();
					quantityCounterMap.put(key, value + count);
					break;

				}
			}

		} else {
			quantityCounterMap.put(id, count);
		}

	}

	public Map<Integer, Integer> getProductQuantityMap() {
		return quantityCounterMap;
	}

	public void deleteProductQuantityFromMap(int id) {
		for (Map.Entry<Integer, Integer> entry : quantityCounterMap.entrySet()) {
			if (entry.getKey() == id) {
				quantityCounterMap.remove(id);
				return;
			}
		}
	}

	public List<Integer> getProductQuantityList(Map<Integer, Integer> quantityCounter) {
		List<Integer> quantities = new LinkedList<Integer>();
		for (Map.Entry<Integer, Integer> entry : quantityCounterMap.entrySet()) {
			quantities.add(entry.getValue());
		}
		return quantities;
	}

}
