package com.internetshop.sequrity;

import java.security.MessageDigest; 
import java.security.NoSuchAlgorithmException;
import java.util.logging.Logger;

/**
 * Class that implementing MD5 algorithm and generate hash of user password
 * 
 * @author Halyna
 * 
 */
public class MD5Encryption {
	private static Logger logger = Logger.getLogger(MD5Encryption.class
			.getName());

	public static String MD5(String text) {
		StringBuffer sb = new StringBuffer();
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
			md.update(text.getBytes());
			byte[] digest = md.digest();

			for (byte b : digest) {
				sb.append(Integer.toHexString((int) (b & 0xff)));
			}

		} catch (NoSuchAlgorithmException e) {
			logger.severe("MD5 password encryption error " + e.getMessage());
		}
		return sb.toString();

	}
}