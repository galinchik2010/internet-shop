<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c"%>

<%@ page contentType = "text/html; charset=utf-8" pageEncoding = "utf-8"%>

<!doctype html public "-//w3c//dtd html 4.01 transitional//en" "http://www.w3.org/tr/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv = "Content-Type" content = "text/html; charset=utf-8" />

        <title>Shop</title>

        <link rel = "shortcut icon" href = "favicon.ico" />

        <link rel = "icon" type = "image/x-icon" href = "favicon.ico" />

        <link rel = "stylesheet" type = "text/css" href = "table_style.css" media = "all">
    </head>

    <body bgcolor = "#CEF6F5">
        <h1 align = "center" style = "color: #04B4AE">Products List</h1>
			<c:if test = "${sessionScope.isOnline == null}">
					<c:set var="notOnline" value="First login!" scope="request"/>
                    <jsp:forward page="login.jsp" /> 
                </c:if>
            <table style = "margin: 0 auto;">
                <thead>
                    <tr>
                        <td>
                            Product id
                        </td>

                        <td>
                            Product
                        </td>

                        <td>
                            Price
                        </td>

                    </tr>
                </thead>

                <tbody>
                    <c:forEach var = "products" items = "${products}" varStatus = "i">
                        <c:choose>
                            <c:when test = "${(i.count) % 2 == 0}">
                                <tr class = "even">
                            </c:when>

                            <c:otherwise>
                                <tr class = "odd">
                            </c:otherwise>
                        </c:choose>

                        <td>
                            ${products.getId()}
                        </td>

                        <td>
                            ${products.getName()}
                        </td>

                        <td>
                            ${products.getPrice()}
                        </td>

                        <td style="border:0;">

                            <a href = "CartServlet?id=${products.getId()}&count=1">
                            <img src = "cart_add.png" /> </a>

                        </td>
                    </c:forEach>
                </tbody>
            </table>
	
        <%--For displaying Page numbers.
            The when condition does not display a link for the current page--%>

        <table border = "1" cellpadding = "5" cellspacing = "5" style = "margin: 0 auto;">
            <tr>

                <%--For displaying Previous link except for the 1st page --%>
                <c:if test = "${currentPage != 1}">
                    <td>
                        <a href = "ShopServlet?currentPage=${currentPage - 1}"
                            style = "cursor:pointer;color: red">Previous</a>
                    </td>
                </c:if>

                <c:forEach begin = "1" end = "${pageNumber}" var = "i">
                    <c:choose>
                        <c:when test = "${currentPage eq i}">
                            <td>
                                ${i}
                            </td>
                        </c:when>

                        <c:otherwise>
                            <td>
                                <a href = "ShopServlet?currentPage=${i}" style = "cursor:pointer;color: red">${i}</a>
                            </td>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>


                <%--For displaying Next link --%>

                <c:if test = "${currentPage lt pageNumber}">
                    <td>
                        <a href = "ShopServlet?currentPage=${currentPage + 1}"
                            style = "cursor:pointer;color: red">Next</a>
                    </td>
                </c:if>
            </tr>
        </table>
         <form method = "post" action = "ShopServlet">
	        <p align = "center">
	        <font size = "2" color = "blue"> <a href = "CartServlet?goToCartPage=yes"><u>see your cart</u></a></font>
	        <input type = "submit" value = "Logout" name = "logout">
        </form>
        <p align = "center" style = "color: #FF0040">
        <c:out value = "${sessionScope.message}" /></p>

        <p align = "center" style = "color: #FF0040">
        <c:out value = "${sessionScope.firstLogOutMessage}" /></p>
    </body>
</html>