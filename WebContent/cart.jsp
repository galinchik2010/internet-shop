<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c"%>

<%@ page contentType = "text/html; charset=utf-8" pageEncoding = "utf-8"%>

<!doctype html public "-//w3c//dtd html 4.01 transitional//en" "http://www.w3.org/tr/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv = "Content-Type" content = "text/html; charset=utf-8" />

        <title>Your cart</title>

        <link rel = "shortcut icon" href = "favicon.ico" />

        <link rel = "icon" type = "image/x-icon" href = "favicon.ico" />

        <link rel = "stylesheet" type = "text/css" href = "table_style.css" media = "all">
    </head>

    <body bgcolor = "#BCF5A9">
    <form method = "post" action = "CartServlet">
        
        <c:if test = "${sessionScope.isOnline == null}">
					<c:set var="notOnline" value="First login!" scope="session"/>
                    <c:redirect url="login.jsp"/>
        </c:if>
        <c:choose>
         <c:when test = "${not empty sessionScope.cartProductsList}">
         <h1 align = "center" style = "color: #886A08">Your cart</h1>
            <table style = "margin: 0 auto;">
                <thead>
                    <tr>
                        <td>
                            Product Id
                        </td>

                        <td>
                            Product
                        </td>

                        <td>
                            Price
                        </td>
                        
                        <td>
                            Quantity
                        </td>
                        
                       
                    </tr>
                </thead>

                <tbody>
                    <c:forEach var = "products" items = "${cartProductsList}" varStatus = "i">
                        <c:choose>
                            <c:when test = "${(i.count) % 2 == 0}">
                                <tr class = "even">
                            </c:when>

                            <c:otherwise>
                                <tr class = "odd">
                            </c:otherwise>
                        </c:choose>

                        <td>
                            ${products.getId()}
                        </td>

                        <td>
                            ${products.getName()}
                        </td>

                        <td>
                            ${products.getPrice()}
                        </td>

                        <td>
                        	<c:forEach var = "ProductQuantityMap" items = "${ProductQuantityMap}">
                        	<c:if test="${products.getId() == ProductQuantityMap.key}">
                        	<c:set var="quantity" value="${ProductQuantityMap.value} "/>
                        	</c:if>
                        	</c:forEach>
                        	
                            <input type="text"   id = "number" size = "2" name = "quantity" value="${quantity}" onkeyup = "check();" readonly >
                    
                        </td>
                        
                        <td style="border:0;">
                          	 <a href = "CartServlet?removeById=${products.getId()}">

                            <img src = "cart_remove.png" /> </a>
                        </td>
                        
                    </c:forEach>
                </tbody>
            </table>
          <p align = "center"> <input type = "submit" id = "checkout" name = "checkout" value = "checkout">
		       			</p>
           </c:when>
            <c:otherwise>
      <h1 align = "center" style = "color: #886A08">Your cart is empty</h1>
    		</c:otherwise>
           	</c:choose>
           	
            	
		           <p align = "center"> <font size = "2" color = "blue"> <a href = "ShopServlet?goToProductsPage=yes"><u>go to products list</u></a></font>
		       			</p>
		       		<p align = "center" style = "color: #088A29">
        				<c:out value = "${requestScope.successfulPurchase}" /></p>
		       		
       		 	  </form>
        <script type = "text/javascript">
        <!--
            function check() {
                var quantity = document.getElementById('number');
				
                var checkout = document.getElementById('checkout');
                quantity.value != ''  
                         ? checkout.disabled = false
                        : checkout.disabled = true;
            }
        	
        //-->
        </script>
    </body>
</html>