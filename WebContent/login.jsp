<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c"%>

<%@ page contentType = "text/html; charset=utf-8" pageEncoding = "utf-8"%>

<!doctype html public "-//w3c//dtd html 4.01 transitional//en" "http://www.w3.org/tr/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv = "Content-Type" content = "text/html; charset=utf-8" />

        <title>Login</title>

        <link rel = "shortcut icon" href = "favicon.ico" />

        <link rel = "icon" type = "image/x-icon" href = "favicon.ico" />
    </head>

    <body bgcolor = "#FFF8DC">
        <form method = "post" action = "LoginServlet">
            <h1 align = "center" style = "color: #FFBF00">Please log in</h1>
             <c:if test = "${sessionScope.isOnline != null}">
					<c:set var="firstLogOutMessage" value="First logout!" scope="session"/>
                    <c:redirect url="ShopServlet"/>
                </c:if>

            <table style = "margin: 0 auto;" cellpadding = "5">
                <tr>
                    <td>
                        Username
                    </td>

                    <td>
                        <input type = "text" name = "username"
                            value = "<c:out value="${requestScope.loginData.getUsername()}"/>" id = "tx_1"
                            onkeyup = "check();" onkeypress = "check();" onchange = "check();" maxlength = "10" />
                    </td>
                </tr>

                <tr>
                    <td>
                        Password
                    </td>

                    <td>
                        <input type = "password" name = "password"
                            value = "<c:out value="${requestScope.loginData.getPassword()}"/>" id = "tx_2"
                            onkeyup = "check();" onkeypress = "check();" onchange = "check();" maxlength = "20" />
                    </td>
                </tr>

                <tr>
                    <td></td>

                    <td>
                        <input type = "submit" value = "login" id = "login" name = "login" disabled = "disabled" />

                        <font size = "2" color = "blue"> <a href = "registration.jsp"><u>Register</u></a></font>
                    </td>
                </tr>

                <tr>
            </table>

            <c:remove var = "user" />

            <br />
        </form>

        <script type = "text/javascript">
        <!--
            function check() {
                var tx_1 = document.getElementById('tx_1');
                var tx_2 = document.getElementById('tx_2');

                var login = document.getElementById('login');
                tx_1.value != '' && tx_2.value != '' 
                         ? login.disabled = false
                        : login.disabled = true;
            }
        //-->
        </script>

        <p align = "center" style = "color: #088A29">
        <c:out value = "${requestScope.successfulRegistrationMessage}" /></p>
        <p align = "center" style = "color: #FF0000">
        <c:out value = "${requestScope.wrongLoginDataMessage}" />
        <c:out value = "${requestScope.technicalProblemsMessage}" />
        <c:out value = "${requestScope.UserNotLoginMessage}" />
        </p>
    </body>
</html>