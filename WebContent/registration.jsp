<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c"%>

<%@ page contentType = "text/html; charset=utf-8" pageEncoding = "utf-8"%>

<!doctype html public "-//w3c//dtd html 4.01 transitional//en" "http://www.w3.org/tr/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv = "Content-Type" content = "text/html; charset=utf-8" />

        <title>Registration</title>

        <link rel = "shortcut icon" href = "favicon.ico" />

        <link rel = "icon" type = "image/x-icon" href = "favicon.ico" />
    </head>

    <body bgcolor = "#ADD8E6">
        <form method = "post" action = "RegistrationServlet">
            <h1 align = "center" style = "color: #0404B4">Please register</h1>

            <table style = "margin: 0 auto;" cellpadding = "5">
                <tr>
                    <td>
                        First Name
                    </td>

                    <td>
                        <input type = "text" name = "firstname" id = "tx_1"
                            value = "<c:out value="${requestScope.user.getFirstname()}"/>" onkeyup = "check();"
                            onkeypress = "check();" onchange = "check();" maxlength = "50" />
                    </td>

                    <td>
                        <font size = "1" color = "grey"> max length 50</font>
                    </td>
                </tr>

                <tr>
                    <td>
                        Last name
                    </td>

                    <td>
                        <input type = "text" name = "lastname"
                            value = "<c:out value="${requestScope.user.getLastname()}"/>" id = "tx_2"
                            onkeyup = "check();" onkeypress = "check();" onchange = "check();" maxlength = "50" />
                    </td>

                    <td>
                        <font size = "1" color = "grey"> max length 50</font>
                    </td>
                </tr>

                <tr>
                    <td>
                        Email
                    </td>

                    <td>
                        <input type = "email" placeholder = "me@example.com" name = "email"
                            value = "<c:out value="${requestScope.user.getEmail()}"/>" id = "tx_3"
                            onkeyup = "check();" onkeypress = "check();" onchange = "check();" maxlength = "50" />
                    </td>

                    <td>
                        <font size = "1" color = "grey"> max length 50</font>
                    </td>
                </tr>

                <tr>
                    <td>
                        Username
                    </td>

                    <td>
                        <input type = "text" name = "username"
                            value = "<c:out value="${requestScope.user.getUsername()}"/>" id = "tx_4"
                            onkeyup = "check();" onkeypress = "check();" onchange = "check();" maxlength = "10" />
                    </td>

                    <td>
                        <font size = "1" color = "grey"> max length 10</font>
                    </td>
                </tr>

                <tr>
                    <td>
                        Password
                    </td>

                    <td>
                        <input type = "password" name = "password"
                            value = "<c:out value="${requestScope.user.getPassword()}"/>" id = "tx_5"
                            onkeyup = "check();" onkeypress = "check();" onchange = "check();" maxlength = "20" />
                    </td>

                    <td>
                        <font size = "1" color = "grey"> max length 20</font>
                    </td>
                </tr>

                <tr>
                    <td></td>

                    <td>
                        <input type = "submit" value = "Registration" id = "registration" name = "Registration"
                            disabled = "disabled" />

                        <font size = "2" color = "blue"> <a href = "login.jsp"><u>Log In</u></a></font>
                    </td>
                </tr>

                <tr>
            </table>

            <c:remove var = "user" />

            <br />
        </form>

        <script type = "text/javascript">
        <!--
            function check() {
                var tx_1 = document.getElementById('tx_1');
                var tx_2 = document.getElementById('tx_2');
                var tx_3 = document.getElementById('tx_3');
                var tx_4 = document.getElementById('tx_4');
                var tx_5 = document.getElementById('tx_5');
                var register = document.getElementById('registration');
                tx_1.value != '' && tx_2.value != '' && tx_3.value != ''
                        && tx_4.value != '' && tx_5.value != ''
                         ? register.disabled = false
                        : register.disabled = true;
            }
        //-->
        </script>

        <p align = "center" style = "color: #FF0040">
        <c:out value = "${requestScope.wrongRegistrationDataMessage}" />
         <c:out value = "${requestScope.technicalProblemsMessage}" /></p>

        <c:remove var = "message" scope = "request" />
</html>